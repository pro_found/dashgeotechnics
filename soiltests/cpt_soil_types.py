#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Bruno Stuyts'

# Django and native Python packages
import base64
import io

# 3rd party packages
import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go
from plotly import tools
import pandas as pd
import numpy as np

# Project imports
from functions import pcpt_normalisations


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css', ]

external_js = ["http://code.jquery.com/jquery-1.9.1.min.js",
               "https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js",
               "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML"]

app = dash.Dash(__name__, external_stylesheets=external_stylesheets, external_scripts=external_js)

app.layout = html.Div(className="page-container", children=html.Div(
    className="main-container", children=html.Div(
        className="main-content", children=[
            html.H1("PCPT interpretation", className="page-title"),
            html.Div(
                className='row', children=[html.Div(
                    className='col-md-6', children=html.Div([
                        html.Div(className="panel-heading clearfix", children=html.Div(
                            "Parameter selection", className="panel-title"
                        )),
                        html.Div(className="panel-body", children=html.Form(className="form-horizontal", children=[
                            html.Div(className="form-group", children=[
                                html.Label('Depth [m]', className="col-sm-2 control-label"),
                                html.Div(className="col-sm-10", children=
                                dcc.Slider(
                                    id='zloc',
                                    min=0,
                                    max=14,
                                    step=0.1,
                                    marks={0: "0", 14: "14"},
                                    value=5,
                                ),)]),
                            html.Div(className='line-dashed'),
                            html.Div(className="form-group", children=[
                                html.Label('Total unit weight [kN/m3]', className="col-sm-2 control-label"),
                                html.Div(className="col-sm-10", children=
                                    dcc.Input(
                                        id='gammatot',
                                        placeholder='Enter a value...',
                                        type='number',
                                        value=19.0,
                                        className='form-control'
                                    ))]),
                            html.Div(className='line-dashed'),
                            html.Div(className="form-group", children=[
                                html.Label('Effective unit weight [kN/m3]', className="col-sm-2 control-label"),
                                html.Div(className="col-sm-10", children=
                                    dcc.Input(
                                        id='gammaeff',
                                        placeholder='Enter a value...',
                                        type='number',
                                        value=9.0,
                                        className='form-control'
                                    ))]),
                            html.Div(className='line-dashed'),
                            html.Div(className="form-group", children=[
                                html.Label('Cone area ratio [-]', className="col-sm-2 control-label"),
                                html.Div(className="col-sm-10", children=
                                    dcc.Input(
                                        id='arearatio',
                                        placeholder='Enter a value...',
                                        type='number',
                                        value=0.7,
                                        className='form-control'
                                    ))]),



                        ]))
                        ], className="panel panel-primary"
                        )
                    ),
                    html.Div(
                        className='col-md-6', children=html.Div(
                            [
                                html.Ul(className="nav nav-tabs", children=[
                                    html.Li(className="active", children=html.A(
                                        "PCPT chart", href="#pcpt", **{'data-toggle': 'tab', 'aria-expanded': 'true'}
                                    )),
                                    html.Li(children=html.A(
                                        "Robertson chart friction", href="#robertson", **{'data-toggle': 'tab', 'aria-expanded': 'false'}
                                    )),
                                    html.Li(children=html.A(
                                        "Robertson chart pore pressure", href="#robertsonbq", **{'data-toggle': 'tab', 'aria-expanded': 'false'}
                                    ))
                                ]),
                                html.Div(className='tab-content', children=[
                                    html.Div(id="pcpt", className="tab-pane active", children=html.Div(
                                        dcc.Graph(id='cptvisualization'), className="panel-body"
                                    )),
                                    html.Div(id="robertson", className="tab-pane", children=html.Div(
                                        dcc.Graph(id='robertsonvisualization'), className="panel-body"
                                    )),
                                    html.Div(id="robertsonbq", className="tab-pane", children=html.Div(
                                        dcc.Graph(id='robertsonvisualizationbq'), className="panel-body"
                                    ))
                                ])
                            ],
                            className='tabs-container'
                        )
                    )
                ]
                ),
                html.Div(children=html.Div([
                html.Div(children=html.Div([
                    dcc.Upload(
                        id='upload-data',
                        children=html.Div([
                            'Drag and Drop or ',
                            html.A('Select Files')
                        ]),
                        style={
                            'width': '100%',
                            'height': '60px',
                            'lineHeight': '60px',
                            'borderWidth': '1px',
                            'borderStyle': 'dashed',
                            'borderRadius': '5px',
                            'textAlign': 'center',
                            'margin': '10px'
                        },
                        # Allow multiple files to be uploaded
                        multiple=False)
                    ], className="col-md-12")
                    )], className="row")
                ),
                html.Div(id='output-data-upload'),
                html.Div(id='intermediate-value', style={'display': 'none'})
            ]
        )
    ))


def parse_contents(contents, filename):
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(
                io.StringIO(decoded.decode('utf-8')))
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])
    return df


@app.callback(Output('intermediate-value', 'children'),
              [Input('upload-data', 'contents')],
              [State('upload-data', 'filename')])
def update_output(contents, filename):
    if contents is not None:
        df = parse_contents(contents, filename)
        return df.to_json()

@app.callback(Output('cptvisualization', 'figure'),
              [Input('intermediate-value', 'children'),
               Input('zloc', 'value')])
def update_graph(jsonified_cleaned_data, zloc):
    try:
        df = pd.read_json(jsonified_cleaned_data).sort_values('z [m]')
    except:
        df = pd.DataFrame()
    try:
        cptfig = tools.make_subplots(rows=1, cols=3, print_grid=False, shared_yaxes=True)
        qc_trace = go.Scatter(
            x=df['qc [MPa]'],
            y=df['z [m]'],
            showlegend=False,
            mode='lines')
        qc_point = go.Scatter(
            x=[np.interp(zloc, df["z [m]"], df["qc [MPa]"])],
            y=[zloc, ],
            showlegend=False,
            mode='markers',
            marker=dict(
                size=10,
                color='rgba(152, 0, 0, .8)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'))
        )
        cptfig.append_trace(qc_trace, 1, 1)
        cptfig.append_trace(qc_point, 1, 1)
        fs_trace = go.Scatter(
            x=df['fs [MPa]'],
            y=df['z [m]'],
            showlegend=False,
            mode='lines')
        fs_point = go.Scatter(
            x=[np.interp(zloc, df["z [m]"], df["fs [MPa]"])],
            y=[zloc, ],
            showlegend=False,
            mode='markers',
            marker=dict(
                size=10,
                color='rgba(152, 0, 0, .8)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'))
        )
        cptfig.append_trace(fs_trace, 1, 2)
        cptfig.append_trace(fs_point, 1, 2)
        try:
            u2_trace = go.Scatter(
                x=df['u2 [MPa]'],
                y=df['z [m]'],
                showlegend=False,
                mode='lines')
            u2_point = go.Scatter(
                x=[np.interp(zloc, df["z [m]"], df["u2 [MPa]"])],
                y=[zloc, ],
                showlegend=False,
                mode='markers',
                marker=dict(
                    size=10,
                    color='rgba(152, 0, 0, .8)',
                    line=dict(
                        width=2,
                        color='rgb(0, 0, 0)'))
            )
        except:
            u2_trace = go.Scatter(
                x=[],
                y=[],
                showlegend=False,
                mode='lines')
            u2_point = go.Scatter(
                x=[],
                y=[],
                showlegend=False,
                mode='lines')
        finally:
            cptfig.append_trace(u2_trace, 1, 3)
            cptfig.append_trace(u2_point, 1, 3)

        cptfig['layout']['xaxis1'].update(title='qc [MPa]', side='top', anchor='y', range=(0.0, 20.0))
        cptfig['layout']['xaxis2'].update(title='fs [MPa]', side='top', anchor='y', range=(0.0, 0.5))
        cptfig['layout']['xaxis3'].update(title='u2 [MPa]', side='top', anchor='y', range=(0.0, 1.0))
        cptfig['layout']['yaxis1'].update(title='Depth [m]', autorange='reversed')
        cptfig['layout'].update(height=900, width=700,
             title='PCPT data',
             margin=dict(t=150, l=100))
        return cptfig
    except Exception as err:
        print(str(err))
        return tools.make_subplots(rows=1, cols=3, print_grid=False)



@app.callback(
    Output('robertsonvisualization', 'figure'),
    [Input('intermediate-value', 'children'),
     Input('zloc', 'value'),
     Input('gammatot', 'value'),
     Input('gammaeff', 'value'),
     Input('arearatio', 'value')
     ])
def update_robertson(jsonified_cleaned_data, zloc, gammatot, gammaeff, arearatio):
    fig = tools.make_subplots(rows=1, cols=1, print_grid=False)
    try:
        df = pd.read_json(jsonified_cleaned_data).sort_values('z [m]')
        qc = np.interp(zloc, df["z [m]"], df["qc [MPa]"])
        fs = np.interp(zloc, df["z [m]"], df["fs [MPa]"])
    except:
        df = pd.DataFrame()
        qc = np.nan
        fs = np.nan
    try:
        u2 = np.interp(zloc, df["z [m]"], df["u2 [MPa]"])
    except:
        u2 = np.nan
    try:
        result = pcpt_normalisations(
            measured_qc=qc,
            measured_fs=fs,
            measured_u2=u2,
            sigma_vo_tot=gammatot * zloc,
            sigma_vo_eff=gammaeff * zloc,
            depth=zloc,
            cone_area_ratio=arearatio)
        resistance_trace = go.Scatter(
            x=[100.0 * result['Fr [-]'], ],
            y=[result['Qt [-]'], ],
            showlegend=False,
            mode='markers',
            marker=dict(
                size=10,
                color='rgba(152, 0, 0, .8)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'))
        )
        fig.append_trace(resistance_trace, 1, 1)
    except Exception as err:
        print("Error during calc - %s" % str(err))
    finally:
        fig['layout']['xaxis1'].update(title='Fr [\%]', range=[-1, 1], type='log')
        fig['layout']['yaxis1'].update(title='Qt', range=(0, 3), type='log')
        fig['layout'].update(
            title='Roberson chart',
            height=700,
            images=[
                dict(
                    source='/assets/robertson.png',
                    xref='x',
                    yref='y',
                    x=-1,
                    y=3,
                    sizex=2,
                    sizey=3,
                    sizing='stretch',
                    opacity=0.7,
                    layer='below',
                )
            ]
        )
        return fig


@app.callback(
    Output('robertsonvisualizationbq', 'figure'),
    [Input('intermediate-value', 'children'),
     Input('zloc', 'value'),
     Input('gammatot', 'value'),
     Input('gammaeff', 'value'),
     Input('arearatio', 'value')
     ])
def update_robertson(jsonified_cleaned_data, zloc, gammatot, gammaeff, arearatio):
    fig = tools.make_subplots(rows=1, cols=1, print_grid=False)
    try:
        df = pd.read_json(jsonified_cleaned_data).sort_values('z [m]')
        qc = np.interp(zloc, df["z [m]"], df["qc [MPa]"])
        fs = np.interp(zloc, df["z [m]"], df["fs [MPa]"])
    except:
        df = pd.DataFrame()
        qc = np.nan
        fs = np.nan
    try:
        u2 = np.interp(zloc, df["z [m]"], df["u2 [MPa]"])
    except:
        u2 = np.nan
    try:
        result = pcpt_normalisations(
            measured_qc=qc,
            measured_fs=fs,
            measured_u2=u2,
            sigma_vo_tot=gammatot * zloc,
            sigma_vo_eff=gammaeff * zloc,
            depth=zloc,
            cone_area_ratio=arearatio)
        resistance_trace = go.Scatter(
            x=[result['Bq [-]'], ],
            y=[result['Qt [-]'], ],
            showlegend=False,
            mode='markers',
            marker=dict(
                size=10,
                color='rgba(152, 0, 0, .8)',
                line=dict(
                    width=2,
                    color='rgb(0, 0, 0)'))
        )
        fig.append_trace(resistance_trace, 1, 1)
    except Exception as err:
        print("Error during calc - %s" % str(err))
    finally:
        fig['layout']['xaxis1'].update(title='Bq [-]', range=[-0.6, 1.4])
        fig['layout']['yaxis1'].update(title='Qt', range=(0, 3), type='log')
        fig['layout'].update(
            title='Roberson chart',
            height=700,
            images=[
                dict(
                    source='/assets/robertsonBq.png',
                    xref='x',
                    yref='y',
                    x=-0.6,
                    y=3,
                    sizex=2,
                    sizey=3,
                    sizing='stretch',
                    opacity=0.7,
                    layer='below',
                )
            ]
        )
        return fig

if __name__ == '__main__':
    app.run_server(debug=True)